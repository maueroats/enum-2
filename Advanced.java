public class Advanced {

    public static void printSimple() {
        for (Simple x: Simple.values()) {
            System.out.println(x);
        }
    }

    public static void printAny(Class<?> e) {
        for (Object x : e.getEnumConstants()) {
            System.out.println(x);
        }
    }

    public static void main (String[] args) {
        Medium x = Medium.valueOf("E");
        System.out.println("E in Medium is: "+
            "["+x+"]"
            +" and has ordinal value "+x.ordinal());

        System.out.println("====== printSimple ======");
        printSimple();

        System.out.println("====== printAny(Medium) ======");
        printAny(Medium.class);
    }

}

