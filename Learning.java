
public class Learning {
    public static String query(Simple x) {
        String result="";

        switch (x) {
            case A: result="you"; break;
            case B: result="got"; break;
            case C: result="it"; break;
        }
        return result;
    }

    public static void main (String[] args) {
        System.out.println( Simple.A );
        System.out.println( "query(B): "+ query(Simple.B) );
        System.out.println( Simple.C );
        System.out.println( Medium.E );
    }

} 
