
public enum Medium {
    D(4), E(5), F(6);

    private int v;

    Medium (int value) {
        v = value;
    }

    public String toString() {
        return "Enum: Medium - " + super.toString() + "(val="+v+")";
    }

    public static void main (String[] args) {
        System.out.println( Medium.valueOf("F") );
    }

}

